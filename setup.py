import os
import sys
import subprocess
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.rst')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

line = CHANGES.splitlines()[0]
version = line.split()[0]

requires=['pyramid',
          'SQLAlchemy',
          'transaction',
          'pyramid_tm',
          'pyramid_debugtoolbar',
          'zope.sqlalchemy',          
          'waitress',
          'ziggurat-foundations',
          'colander',
          'deform',
          'pyramid_chameleon',
          'psycopg2-binary',
          'alembic',
          'pyramid_beaker',
          'pytz',
          'paste',       
          'paginate_sqlalchemy',
          'pyramid_rpc',
          'requests',
          'tailer',
          'pyramid_mailer',
          'bcrypt',
         ]


def pip_install(package, upgrade=False):
    cmd = [pip, 'install']
    if upgrade:
        cmd += ['--upgrade']
    if sys.argv[2:]:
        option = sys.argv[2]  # Bisa untuk proxy
        cmd += [option]
    cmd += [package]
    subprocess.call(cmd)


def pip_upgrade(package):
    pip_install(package, True)


if sys.argv[1:] and sys.argv[1] == 'develop-use-pip':
    bin_ = os.path.split(sys.executable)[0]
    pip = os.path.join(bin_, 'pip')
    pip_upgrade('pip')
    pip_upgrade('setuptools')
    for package in requires:
        pip_install(package)
    cmd = [sys.executable, sys.argv[0], 'develop']
    subprocess.call(cmd)
    sys.exit()


setup(name='daemon_agent',
      version=version,
      description='daemon_agent',
      long_description=README + '\n\n' +  CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pylons",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='',
      author_email='',
      url='',
      keywords='web pyramid pylons',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      tests_require=requires,
      test_suite="daemon_agent",
      entry_points = """\
      [paste.app_factory]
      main = daemon_agent:main
      [console_scripts]
      initialize_daemon_agent_db = daemon_agent.scripts.initializedb:main      
      hello = daemon_agent.scripts.TestClient:main
      tambahkan = daemon_agent.scripts.TestClient:tambahkan
      netcheck = daemon_agent.scripts.netcheck:main
      """,
      )
