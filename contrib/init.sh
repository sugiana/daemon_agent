#!/bin/sh

### BEGIN INIT INFO
# Provides:          daemon-agent 
# Required-Start:    cron 
# Required-Stop:     cron 
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6 
# Short-Description: Daemon Monitoring System 
# Description:       Pyramid application for monitor daemon activity
### END INIT INFO


DESC="Daemon Agent"
BIN="/usr/local/bin/daemon-agent"
PID="/var/run/daemon-agent.pid"

. /lib/lsb/init-functions

case $1 in
  start)
    log_begin_msg "Starting $DESC"
    /sbin/start-stop-daemon --background --pidfile $PID --startas $BIN --start
    log_end_msg $?
    ;;
    
  stop)
    log_begin_msg "Stopping $DESC"
    /sbin/start-stop-daemon --pidfile $PID --stop
    log_end_msg $?
    ;;
    
  restart)
    $0 stop
    sleep 3
    $0 start
    ;;
    
  *) echo "Usage: $0 {start|stop|restart}"
esac

exit 0
