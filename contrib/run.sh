#!/bin/sh

cd /home/husein/daemon_agent
../env/bin/pserve test.ini --reload >/dev/null 2>&1 &
echo $! > /var/run/daemon-agent.pid
