cd /usr/local/web/daemon_agent
../env/bin/initialize_daemon_agent_db live.ini
cp contrib/init.sh /etc/init.d/daemon-agent
chmod 755 /etc/init.d/daemon-agent
cp contrib/cron /etc/cron.d/daemon-agent
cp contrib/logrotate /etc/logrotate.d/daemon-agent
cd /etc/rc2.d
ln -s ../init.d/daemon-agent S70daemon-agent 
/etc/init.d/daemon-agent start
