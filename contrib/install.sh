cd /usr/local/web
virtualenv env
cd daemon_agent
../env/bin/python setup.py develop-use-pip
echo "createuser -P web"
su - postgres -c "createuser -P web"
echo "createdb -O web daemon_agent"
su - postgres -c "createdb -O web daemon_agent"
cp production.ini live.ini
echo "Sesuaikan database pada live.ini, lalu jalankan:"
echo
echo "sh contrib/configure.sh"
echo
