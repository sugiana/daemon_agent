"""security_code_date with time zone

Revision ID: be75f4217ec7
Revises: 35f453a443
Create Date: 2019-01-19 04:28:57.324741

"""

# revision identifiers, used by Alembic.
revision = 'be75f4217ec7'
down_revision = '35f453a443'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.alter_column('users', 'security_code_date',
        type_=sa.DateTime(timezone=True),
        existing_type=sa.DateTime(timezone=False))


def downgrade():
    pass
