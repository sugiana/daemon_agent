from ziggurat_foundations.models.services import BaseService 
from ..conf import Conf


class ConfService(BaseService):    
    model = Conf

    @classmethod
    def get(cls, grup, nama, db_session=None):
        q = cls.base_query(db_session)
        q = q.filter_by(grup=grup, nama=nama)
        return q.first()
 
    @classmethod
    def to_list(cls, instance):
        return instance.nilai.split()

    @classmethod
    def to_bool(cls, instance):
        return instance.nilai.strip().lower() == 'true'

    @classmethod
    def get_val(cls, grup, nama, default=None, db_session=None):
        r = cls.get(grup, nama, db_session)
        if not r:
            return default
        if default is None:
            return r.nilai
        if isinstance(default, int):
            return int(r.nilai)
        if isinstance(default, float):
            return float(r.nilai)
        if isinstance(default, list):
            return cls.to_list(r)
        if isinstance(default, bool):
            return cls.to_bool(r)
        return r.nilai

    @classmethod
    def get_int(cls, grup, nama):
        row = get(cls, grup, nama)
        if row is not None:
            return int(row.nilai)

    @classmethod
    def get_float(cls, grup, nama):
        row = get(cls, grup, nama)
        if row is not None:
            return float(row.nilai)

    @classmethod
    def get_list(cls, grup, nama):
        row = get(cls, grup, nama)
        if row is not None:
            return cls.to_list(row)

    @classmethod
    def get_bool(cls, grup, nama):
        row = get(cls, grup, nama)
        if row is not None:
            return cls.to_bool(row)
