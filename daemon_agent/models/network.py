from sqlalchemy import (
    Column,
    Integer,
    String,
    DateTime,
    ForeignKey,
    )
from sqlalchemy.orm import relationship
from ..tools.waktu import create_now
from . import (
    Base,
    CommonModel,
    DefaultModel,
    )
    
    
class NetStatus(DefaultModel, Base):
    __tablename__ = 'net_status'
    description = Column(String(64), nullable=False, unique=True)
    

# Konektivitas di tempat lain yang dilaporkan ke sini
class NetConnection(DefaultModel, Base):
    __tablename__ = 'net_connection'
    name = Column(String(16), nullable=False, unique=True)
    description = Column(String(64))
    ip = Column(String(15), nullable=False)
    port = Column(Integer)
    gateway = Column(String(16)) # references (net_connection.name)
    status_id = Column(Integer, ForeignKey('net_status.id'), nullable=False)
    status = relationship('NetStatus')
    updated = Column(DateTime(timezone=True), nullable=False)


class NetLog(DefaultModel, Base):
    __tablename__ = 'net_log'
    created = Column(DateTime(timezone=True), nullable=False,
                default=create_now)
    name = Column(String(16), nullable=False)
    ip = Column(String(15), nullable=False)
    port = Column(Integer)
    status_id = Column(Integer, ForeignKey('net_status.id'), nullable=False)
