import os
import re
import time
import tailer
import sys
import subprocess
from shutil import copyfile
from sqlalchemy import (
    Column,
    String,
    Integer,
    Text,
    DateTime,
    ForeignKey,
    UniqueConstraint,
    )
from . import (
    Base,
    DefaultModel,
    )
from ..tools.daemon import (
    get_pid,
    is_live,
    )
from ..tools.waktu import create_datetime
from ..dpkg import version as get_debian_version


REGEX_LOG = '^([\d]*)-([\d]*)-([\d]*) ([\d]*):([\d]*):([\d]*),([\d]*) '\
            '(INFO|ERROR) (.*)'
REGEX_LOG = re.compile(REGEX_LOG)


def epoch_to_tz(epoch):
    t = time.localtime(epoch)
    return create_datetime(t.tm_year, t.tm_mon, t.tm_mday,
            t.tm_hour, t.tm_min, t.tm_sec)


# Thread juga dicatat di sini.
class Daemon(Base, DefaultModel):
    __tablename__ = 'daemon'
    name = Column(String(32), nullable=False, unique=True)
    command_start = Column(String(128))
    command_stop = Column(String(128))
    # Untuk memeriksa kapan daemon hidup.
    pid_file = Column(String(128))
    # Untuk memeriksa apakah daemon terpasang.
    bin_file = Column(String(128))
    log_file = Column(String(128))
    description = Column(Text)
    # Untuk menampilkan versi
    debian_name = Column(String(32))

    ##################
    # Is installed ? #
    ##################
    def get_bin_file(self):
        return self.bin_file

    def is_installed(self):
        if self.debian_name:
            return self.get_debian_version()
        if not self.bin_file:
            return
        bin_file = self.get_bin_file()
        if os.path.exists(bin_file):
            return True
        return False 

    def get_debian_version(self):
        return get_debian_version(self.debian_name)

    def is_installed_report(self):
        result = self.is_installed()
        if result is None:
            return
        if result is False:
            return 'tidak'
        message = 'ya'
        if self.debian_name:
            return message + ', versi {v}'.format(v=result)
        return message

    #############
    # Is live ? #
    #############
    def get_pid_file(self):
        return self.pid_file

    def is_live(self):
        if not self.pid_file :
            return
        pid_file = self.get_pid_file()
        pid = get_pid(pid_file)
        if not pid:
            return
        if not is_live(pid):
            return False
        stat = os.stat(pid_file)
        return epoch_to_tz(stat.st_mtime)

    def is_live_report(self):
        result = self.is_live()
        if result is None:
            return
        if result is False:
            return 'tidak'
        return result.strftime('%d-%m-%Y %H:%M:%S')

    #################
    # Last log time #
    #################
    def get_log_file(self):
        return self.log_file

    def last_log_time(self):
        if not self.log_file:
            return
        log_file = self.get_log_file()
        if not os.path.exists(log_file):
            return
        # Sengaja dibuat banyak sampai 50 baris karena saat ada raise exception
        # tanggal tidak tertulis.
        f = open(log_file)
        lines = tailer.tail(f, 50)
        f.close()
        if not lines:
            return 0
        lines.reverse()
        match = None
        for line in lines:
            match = REGEX_LOG.search(line)
            if match:
                break
        if not match:
            return False
        year = int(match.group(1))
        month = int(match.group(2))
        day = int(match.group(3))
        hour = int(match.group(4))
        minute = int(match.group(5))
        second = int(match.group(6))
        milisecond = int(match.group(7))
        return create_datetime(year, month, day, hour, minute, second,
                milisecond)
                
    def last_log_report(self):
        result = self.last_log_time()
        if result is None:
            return
        if result is 0: 
            return 'kosong'
        if result is False:
            return 'awalan waktu tidak ditemukan'
        return result.strftime('%d-%m-%Y %H:%M:%S')


# Struktur ini dari https://github.com/opensipkd/iso8583-forwarder
class Jenis(Base):
    __tablename__ = 'log_jenis'
    id = Column(Integer, primary_key=True)
    nama = Column(String(16), nullable=False, unique=True)


class Kategori(Base):
    __tablename__ = 'log_kategori'
    id = Column(Integer, primary_key=True)
    nama = Column(String(7), nullable=False, unique=True)


class Log(Base):
    __tablename__ = 'log'
    id = Column(Integer, primary_key=True)
    created = Column(DateTime(timezone=True), nullable=False)
    jenis_id = Column(Integer, ForeignKey(Jenis.id), nullable=False)
    line = Column(Text, nullable=False)
    line_id = Column(String(32), nullable=False)
    tgl = Column(DateTime(timezone=True), nullable=False)
    kategori_id = Column(Integer, ForeignKey(Kategori.id), nullable=False)
    __table_args__ = (
        UniqueConstraint('jenis_id', 'line_id'),)
