import sqlalchemy as sa
from sqlalchemy.orm import (
    sessionmaker,
    scoped_session,
    )
from sqlalchemy.ext.declarative import (
    declarative_base,
    declared_attr,
    )
from zope.sqlalchemy import ZopeTransactionExtension
from ziggurat_foundations.models.base import BaseModel


DBSession = scoped_session(
    sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()


class CommonModel(BaseModel):
    to_dict = BaseModel.get_dict
    from_dict = BaseModel.populate_obj

    def to_dict_without_none(self):
        values = {}
        for k in self._get_keys():
            val = getattr(self, k)
            if val is not None:
                values[k] = val
        return values


class DefaultModel(CommonModel):
    @declared_attr
    def id(self):
        return sa.Column(sa.Integer, primary_key=True, autoincrement=True)
