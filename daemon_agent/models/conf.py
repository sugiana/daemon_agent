from sqlalchemy import (
    Column,
    String,
    Text,
    )
from . import (
    Base,
    CommonModel,
    )


class Conf(Base, CommonModel):
    __tablename__ = 'conf'
    grup = Column(String(64), nullable=False, primary_key=True)
    nama = Column(String(64), nullable=False, primary_key=True)
    nilai = Column(Text, nullable=False)
    ket = Column(Text)
