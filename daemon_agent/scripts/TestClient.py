import sys
import json
from requests import post
from pprint import pprint


BASE_DATA = dict(
    jsonrpc='2.0',
    id=1,
    )


def main(argv=sys.argv):
    if argv[1:]:
        url = argv[1]
    else:
        url = 'http://localhost:6543/api'
    data = dict(BASE_DATA)
    data.update(dict(
        method='say_hello',
        params=['sugiana'],
        ))
    headers = dict()
    print('URL: {url}'.format(url=url))
    print('Input:')
    pprint(data)
    json_data = json.dumps(data)
    r = post(url, data=json_data, headers=headers)
    print('Output:')
    resp = json.loads(r.text)
    print(resp)
    
def tambahkan(argv=sys.argv):
    args = argv[1:]
    url = 'http://localhost:6543/api'
    if args[1:]:
        if args[-1].find('http') > -1:
            url = args[-1]
            args = args[:-1]
        args = [float(x) for x in args]
    else:
        args = [1, 2]
    data = dict(BASE_DATA)
    data.update(dict(
        method='tambahkan',
        params=[args],
        ))
    headers = dict()
    print('URL: {url}'.format(url=url))
    print('Input:')
    pprint(data)
    json_data = json.dumps(data)
    r = post(url, data=json_data, headers=headers)
    print('Output:')
    resp = json.loads(r.text)
    print(resp)
