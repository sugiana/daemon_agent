import os
import sys
from ..tools.this_framework import get_settings
from ..tools.daemon import (
    get_pid,
    is_live,
    )


def get_pid_file(pid_name=None):
    if not pid_name:
        pid_name = os.path.split(sys.argv[0])[-1]
        pid_name = os.path.splitext(pid_name)[0]
    settings = get_settings()
    dir_path = settings['session.lock_dir']
    return os.path.join(settings['session.lock_dir'], pid_name)
            

def mkdir(dir_name):
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)
        

def get_fullpath(filename):
    dir_name = os.path.split(__file__)[0]
    return os.path.join(dir_name, filename)
