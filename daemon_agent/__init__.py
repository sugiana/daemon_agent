import sys
import os
import csv
if sys.version_info.major == 2:
    from urllib import (
        urlencode,
        quote,
        quote_plus,
        )    
else:
    from urllib.parse import (
        urlencode,
        quote,
        quote_plus,
        )
import deform
from pkg_resources import resource_filename
from pyramid.config import Configurator
from pyramid_beaker import session_factory_from_settings
from pyramid.i18n import get_localizer
from pyramid.threadlocal import get_current_request
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.events import subscriber
from pyramid.events import BeforeRender
from sqlalchemy import engine_from_config
from .security import (
    group_finder,
    get_user,
    )
from .models import DBSession
from .views import RemoveSlashNotFoundViewFactory
from .tools.this_framework import get_locale_name


here = os.path.abspath(os.path.dirname(__file__))
routes_file = os.path.join(here, 'routes.csv')


def set_paths(config):
    with open(routes_file) as f:
        c = csv.DictReader(f)
        for row in c:
            path = row['path'] or '/' + row['name']
            config.add_route(row['name'], path)
    config.scan()


# https://groups.google.com/forum/#!topic/pylons-discuss/QIj4G82j04c
def has_permission_(request, perm_names):
    if isinstance(perm_names, str):
        return request.has_permission(perm_names)
    for perm_name in perm_names:
        if request.has_permission(perm_name):
            return True


@subscriber(BeforeRender)
def add_global(event):
     event['has_permission'] = has_permission_
     event['urlencode'] = urlencode
     event['quote_plus'] = quote_plus
     event['quote'] = quote   


def translator(term):
    return get_localizer(get_current_request()).translate(term)


deform_template_dir = resource_filename('deform', 'templates/')
zpt_renderer = deform.ZPTRendererFactory(
        [deform_template_dir], translator=translator)
deform.Form.set_default_renderer(zpt_renderer)


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)

    session_factory = session_factory_from_settings(settings)
    config = Configurator(
        settings=settings,
        root_factory='daemon_agent.models.ziggurat.RootFactory',
        session_factory=session_factory,
        locale_negotiator=get_locale_name)
    config.include('pyramid_beaker')                          
    config.include('pyramid_chameleon')
    config.include('pyramid_rpc.jsonrpc')

    authn_policy = AuthTktAuthenticationPolicy('sosecret',
                    callback=group_finder, hashalg='sha512')
    authz_policy = ACLAuthorizationPolicy()                          
    config.set_authentication_policy(authn_policy)
    config.set_authorization_policy(authz_policy)
    config.add_request_method(get_user, 'user', reify=True)
    config.add_notfound_view(RemoveSlashNotFoundViewFactory())        
                          
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_static_view('deform_static', 'deform:static')
    config.add_static_view('files', settings['static_files'])    
    config.add_translation_dirs('locale')
    config.add_jsonrpc_endpoint('api', '/api')
    set_paths(config)
    return config.make_wsgi_app()
