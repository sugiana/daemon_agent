from pyramid_rpc.jsonrpc import jsonrpc_method


@jsonrpc_method(endpoint='api')
def say_hello(request, name):
    s = 'Hello {n}'.format(n=name)
    return dict(code=0, message=s)

@jsonrpc_method(endpoint='api')
def tambahkan(request, numbers):
    total = 0
    for number in numbers:
        total += number
    return dict(code=0, message='OK', total=total)
