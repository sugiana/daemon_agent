from sqlalchemy import distinct
from pyramid.view import view_config
from pyramid.httpexceptions import (
    HTTPFound,
    HTTPNotFound,
    )
import colander
from deform import (
    Form,
    widget,
    ValidationFailure,
    Button,
    )
from ..models import DBSession
from ..models.conf import Conf


########                    
# List #
########    
@view_config(
    route_name='conf', renderer='templates/conf/list.pt',
    permission='conf-edit')
def view_list(request):
    resp = dict(title='Konfigurasi')
    resp['rows'] = q = DBSession.query(distinct(Conf.grup).label('grup')).order_by(Conf.grup)
    resp['count'] = q.count()
    return resp


@view_config(
    route_name='conf-grup', renderer='templates/conf/grup.pt',
    permission='conf-edit')
def view_grup_list(request):
    grup = request.matchdict['grup']
    resp = dict(title='Konfigurasi '.format(grup))
    resp['rows'] = DBSession.query(Conf).filter_by(grup=grup).order_by(Conf.nama)
    return resp


#######    
# Add #
#######

class AddSchema(colander.Schema):
    grup = colander.SchemaNode(colander.String())
    nama = colander.SchemaNode(colander.String())
    nilai = colander.SchemaNode(colander.String(),
                widget=widget.TextAreaWidget(rows=5))
    ket = colander.SchemaNode(colander.String(),
            missing=colander.drop,
            widget=widget.TextAreaWidget(rows=5),
            title='Keterangan')


def add_form_validator(form, value):
    q = DBSession.query(Conf).filter_by(
            grup=value['grup'], nama=value['nama'])
    row = q.first()
    if row:
        title = get_title(row)
        msg = '{} sudah ada.'.format(title)
        raise colander.Invalid(form, msg)
                    

def get_form(request, class_form, validator=None):
    schema = class_form(validator=validator)
    btn_save = Button('save', 'Simpan')
    btn_cancel = Button('cancel', 'Batal')
    buttons = (btn_save, btn_cancel)
    return Form(schema, buttons=buttons)
    

def insert(values):
    row = Conf()
    row.from_dict(values)
    DBSession.add(row)
    DBSession.flush()
    return row
    

def route_list(request):
    return HTTPFound(location=request.route_url('conf'))
    

def route_grup_list(request, row):
    return HTTPFound(location=request.route_url('conf-grup', grup=row.grup))
    

def get_title(row):
    return 'Konfigurasi {} - {}'.format(row.grup, row.nama)

    
@view_config(
    route_name='conf-add', renderer='templates/conf/add.pt',
    permission='conf-edit')
def view_add(request):
    form = get_form(request, AddSchema, add_form_validator)
    resp = dict(title='Tambah konfigurasi')
    if not request.POST:
        resp['form'] = form.render()
        return resp
    if 'save' not in request.POST:
        return route_list(request)
    items = request.POST.items()
    try:
        c = form.validate(items)
    except ValidationFailure as e:
        resp['form'] = e.render()
        return resp
    values = dict(c.items())
    row = insert(values)
    title = get_title(row)
    msg = '{} sudah disimpan'.format(title)
    request.session.flash(msg)
    return route_grup_list(request, row) 


########
# Edit #
########
def query_id(request):
    return DBSession.query(Conf).filter_by(
            grup=request.matchdict['grup'],
            nama=request.matchdict['nama'])

    
class EditSchema(colander.Schema):
    grup = colander.SchemaNode(colander.String(),
            missing=colander.drop,
            widget=widget.TextInputWidget(readonly=True))
    nama = colander.SchemaNode(
            colander.String(), missing=colander.drop,
            widget=widget.TextInputWidget(readonly=True))
    nilai = colander.SchemaNode(colander.String(),
                widget=widget.TextAreaWidget(rows=10, cols=60))
    ket = colander.SchemaNode(colander.String(),
            missing=colander.drop,
            widget=widget.TextAreaWidget(rows=10, cols=60),
            title='Keterangan')


def update(row, values):
    row.from_dict(values)
    DBSession.add(row)


@view_config(
    route_name='conf-edit', renderer='templates/conf/edit.pt',
    permission='conf-edit')
def view_edit(request):
    q = query_id(request)
    row = q.first()
    if not row:
        return HTTPNotFound()
    title = get_title(row)
    resp = dict(title=title)
    form = get_form(request, EditSchema)
    if not request.POST:
        resp['row'] = row
        resp['form'] = form.render(appstruct=row.to_dict_without_none())
        return resp
    if 'save' not in request.POST:
        return route_grup_list(request, row)
    items = request.POST.items()
    try:
        c = form.validate(items)
    except ValidationFailure as e:
        resp['form'] = e.render()
        return resp
    update(row, dict(c.items()))
    title = get_title(row)
    msg = '{} sudah diperbarui'.format(title)
    request.session.flash(msg)
    return route_grup_list(request, row)


##########
# Delete #
##########    
@view_config(
    route_name='conf-delete', renderer='templates/conf/delete.pt',
    permission='conf-edit')
def view_delete(request):
    q = query_id(request)
    row = q.first()
    if not row:
        return HTTPNotFound()
    btn_delete = Button('delete', 'Hapus')
    btn_cancel = Button('cancel', 'Batal')
    buttons = (btn_delete, btn_cancel)
    form = Form(colander.Schema(), buttons=buttons)
    title = get_title(row)
    resp = dict(title=title)
    if not request.POST:
        resp['row'] = row
        resp['form'] = form.render()
        return resp
    if 'delete' not in request.POST:
        return route_grup_list(request, row)
    msg = '{} berhasil dihapus.'.format(title)
    q.delete()
    DBSession.flush()
    q = DBSession.query(Conf).filter_by(grup=request.matchdict['grup'])
    row = q.first()
    request.session.flash(msg)
    if row:
        return route_grup_list(request, row)
    return route_list(request)
