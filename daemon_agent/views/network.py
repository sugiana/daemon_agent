from pyramid.view import view_config
from pyramid.httpexceptions import (
    HTTPFound,
    HTTPNotFound,
    )
import colander
from deform import (
    Form,
    widget,
    ValidationFailure,
    Button,
    )
from ..models import DBSession
from ..models.network import NetConnection
from ..tools.waktu import create_now


WAITING = 1 # Tabel net_status

 
########                    
# List #
########    
@view_config(
    route_name='network', renderer='templates/network/list.pt',
    permission='view')
def view_list(request):
    resp = dict(title='Jaringan')
    resp['rows'] = rows = DBSession.query(NetConnection).order_by(NetConnection.name)
    resp['count'] = rows.count() 
    return resp


#######    
# Add #
#######               
HELP_NAME = 'Gunakan nama host ini saat Anda mengisikan Gateway di host lainnya '\
        'nanti. Contoh: Mikrotik'
HELP_DESCRIPTION = 'Ini digunakan juga saat memperbarui status di mesin induk'
HELP_GATEWAY = 'Gunakan nama host lainnya'


class Schema(colander.Schema):
    name = colander.SchemaNode(
            colander.String(), title='Nama', description=HELP_NAME,
            validator=colander.Length(max=16))
    ip = colander.SchemaNode(colander.String(), title='IP')
    port = colander.SchemaNode(colander.String(), missing=colander.drop)
    description = colander.SchemaNode(
            colander.String(), missing=colander.drop,
            description=HELP_DESCRIPTION, title='Keterangan')
    gateway = colander.SchemaNode(
            colander.String(), missing=colander.drop, description=HELP_GATEWAY)
    

def get_form():
    schema = Schema()
    btn_save = Button('save', 'Simpan')
    btn_cancel = Button('cance', 'Batalkan')
    buttons = (btn_save, btn_cancel)
    return Form(schema, buttons=buttons)


def save(row):
    row.updated = create_now()
    DBSession.add(row)


def insert(values):
    row = NetConnection()
    row.from_dict(values)
    row.status_id = WAITING
    save(row)
 
        
@view_config(
    route_name='network-add', renderer='templates/network/add.pt',
    permission='view')
def view_add(request):
    form = get_form()
    resp = dict(title='Tambah sambungan jaringan')
    if not request.POST:
        resp['form'] = form.render()
        return resp
    if 'save' not in request.POST:
        return route_list(request)
    items = request.POST.items()
    try:
        c = form.validate(items)
    except ValidationFailure as e:
        resp['form'] = e.render()
        return resp
    values = dict(c.items())
    insert(values) 
    msg = 'Host {} sudah ditambahkan.'.format(values['name'])
    request.session.flash(msg)
    return route_list(request)
    

########
# Edit #
########
def update(row, values):
    row.from_dict(values)
    save(row)

        
def route_list(request):
    return HTTPFound(location=request.route_url('network'))
    
 
def query_id(request):
    return DBSession.query(NetConnection).filter_by(id=request.matchdict['id'])
    

@view_config(
    route_name='network-edit', renderer='templates/network/edit.pt',
    permission='view')
def view_edit(request):
    q = query_id(request)
    row = q.first()
    if not row:
        return HTTPNotFound()
    form = get_form()
    resp = dict(title='Ubah sambungan jaringan')
    if not request.POST:
        resp['form'] = form.render(appstruct=row.to_dict_without_none())
        return resp
    if 'save' not in request.POST:
        return route_list(request)
    items = request.POST.items()
    try:
        c = form.validate(items)
    except ValidationFailure as e:
        resp['form'] = e.render()
        return resp
    values = dict(c.items())
    update(row, values)
    msg = 'Jaringan {} sudah diperbarui.'.format(row.description)
    request.session.flash(msg)
    return route_list(request)


##########
# Delete #
##########    
@view_config(
    route_name='network-delete', renderer='templates/network/delete.pt',
    permission='view')
def view_delete(request):
    q = query_id(request)
    row = q.first()
    if not row:
        return HTTPNotFound()
    schema = colander.Schema()
    btn_delete = Button('delete', 'Hapus')
    btn_cancel = Button('cancel', 'Tidak jadi hapus')
    buttons = (btn_delete, btn_cancel)
    form = Form(schema, buttons=buttons)
    resp = dict(title='Hapus sambungan jaringan')
    if not request.POST:
        resp['row'] = row
        resp['form'] = form.render()
        return resp
    if 'delete' not in request.POST:
        return route_list(request)
    msg = 'Host {} sudah dihapus.'.format(row.name)
    q.delete()
    DBSession.flush()
    request.session.flash(msg)
    return route_list(request)
