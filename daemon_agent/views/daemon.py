from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
import colander
from deform import (
    Form,
    widget,
    ValidationFailure,
    Button,
    )
from ..models import DBSession
from ..models.logs import Daemon


########                    
# List #
########    
@view_config(
    route_name='daemon', renderer='templates/daemon/list.pt',
    permission='view')
def view_list(request):
    resp = dict(title='Daemon')
    q = DBSession.query(Daemon)
    resp['found'] = q.first()
    resp['rows'] = q.order_by(Daemon.name)
    return resp


#######    
# Add #
#######
class AddNameValidator:
    def __call__(self, node, value):
        q = DBSession.query(Daemon).filter(Daemon.name.ilike(value))
        row = q.first()
        if row:
            msg = 'Nama {} sudah digunakan'.format(value)
            raise colander.Invalid(node, msg)


@colander.deferred
def add_name_validator(node, kw):
    return AddNameValidator()


def node_name(validator):
    return colander.SchemaNode(
            colander.String(), description='Contoh: H2H PBB',
            validator=validator)

 
def node_debian_name():
    return colander.SchemaNode(
            colander.String(), missing=colander.drop,
            description='Contoh: opensipkd-pbb')


def node_bin_file():
    return colander.SchemaNode(
            colander.String(), missing=colander.drop,
            description='Contoh: /usr/bin/opensipkd/pbbm_sppt_ora. Digunakan '\
                        'untuk menampilkan status terpasang atau tidak. '\
                        'Kosongkan bila Debian Name sudah diisi.')

def node_command_start():
    return colander.SchemaNode(
            colander.String(), missing=colander.drop,
            description='Contoh: /etc/init.d/iso8583-forwarder start')


def node_command_stop():
    return colander.SchemaNode(
            colander.String(), missing=colander.drop,
            description='Contoh: /etc/init.d/iso8583-forwarder stop')


def node_pid_file():
    return colander.SchemaNode(
            colander.String(), missing=colander.drop,
            description='Contoh: /var/run/iso8583-forwarder.pid')


def node_log_file():
    return colander.SchemaNode(
            colander.String(), missing=colander.drop,
            description='Contoh: /var/log/iso8583-forwarder/main.log')

    
def node_description():
    return colander.SchemaNode(
            colander.String(), missing=colander.drop,
            widget=widget.TextAreaWidget(rows=5),
            description='Keterangan tambahan')


class AddSchema(colander.Schema):
    name = node_name(add_name_validator)
    debian_name = node_debian_name()
    bin_file = node_bin_file()
    command_start = node_command_start()
    command_stop = node_command_stop()
    pid_file = node_pid_file()
    log_file = node_log_file()
    description = node_description()


def get_buttons():
    btn_save = Button('save', 'Simpan')
    btn_cancel = Button('cancel', 'Batal')
    return (btn_save, btn_cancel)


def insert(values):
    row = Daemon()
    row.from_dict(values)
    DBSession.add(row)
    DBSession.flush()
    return row

    
def route_list(request):
    return HTTPFound(location=request.route_url('daemon'))
    
    
@view_config(
    route_name='daemon-add', renderer='templates/daemon/add.pt',
    permission='view')
def view_add(request):
    schema = AddSchema()
    schema = schema.bind()
    form = Form(schema, buttons=get_buttons())
    resp = dict(title='Tambah daemon')
    if not request.POST:
        resp['form'] = form.render()
        return resp
    if 'save' not in request.POST:
        return route_list(request)
    items = request.POST.items()
    try:
        c = form.validate(items)
    except ValidationFailure as e:
        resp['form'] = e.render()
        return resp
    row = insert(dict(c.items()))
    msg = 'Daemon {} sudah ditambahkan'.format(row.name)
    request.session.flash(msg)
    return route_list(request)


########
# Edit #
########
class EditNameValidator:
    def __init__(self, daemon_id):
        self.daemon_id = daemon_id

    def __call__(self, node, value):
        q = DBSession.query(Daemon).filter(Daemon.name.ilike(value))
        row = q.first()
        if not row:
            return
        if row.id != self.daemon_id:
            msg = 'Nama {} sudah digunakan'.format(value)
            raise colander.Invalid(node, msg)


@colander.deferred
def edit_name_validator(node, kw):
    return EditNameValidator(kw['id'])


class EditSchema(colander.Schema):
    id = colander.SchemaNode(
            colander.String(), missing=colander.drop,
            widget=widget.HiddenWidget(readonly=True))
    name = node_name(edit_name_validator)
    debian_name = node_debian_name()
    bin_flie = node_bin_file()
    command_start = node_command_start()
    command_stop = node_command_stop()
    pid_file = node_pid_file()
    log_file = node_log_file()
    description = node_description()


def query_id(request):
    return DBSession.query(Daemon).filter_by(id=request.matchdict['id'])


def update(row, values):
    row.from_dict(values)
    DBSession.add(row)
    DBSession.flush()
    

@view_config(
    route_name='daemon-edit', renderer='templates/daemon/edit.pt',
    permission='view')
def view_edit(request):
    q = query_id(request)
    row = q.first()
    if not row:
        return HTTPNotFound()
    schema = EditSchema()
    schema = schema.bind(id=int(request.matchdict['id']))
    form = Form(schema, buttons=get_buttons())
    resp = dict(title='Ubah {}'.format(row.name))
    if not request.POST:
        resp['form'] = form.render(appstruct=row.to_dict_without_none())
        return resp
    if 'save' not in request.POST:
        return route_list(request)
    items = request.POST.items()
    try:
        c = form.validate(items)
    except ValidationFailure as e:
        resp['form'] = e.render()
        return resp
    values = dict(c.items())
    update(row, values)
    msg = 'Daemon {} sudah diperbarui'.format(row.name)
    request.session.flash(msg)
    return route_list(request)


##########
# Delete #
##########    
@view_config(
    route_name='daemon-delete', renderer='templates/daemon/delete.pt',
    permission='view')
def view_delete(request):
    q = query_id(request)
    row = q.first()
    if not row:
        return HTTPNotFound()
    btn_delete = Button('delete', 'Hapus')
    btn_cancel = Button('cancel', 'Batal')
    buttons = (btn_delete, btn_cancel)
    form = Form(colander.Schema(), buttons=buttons)
    resp = dict(title='Hapus {}'.format(row.name))
    if not request.POST:
        resp['form'] = form.render()
        resp['row'] = row
        return resp
    if 'delete' not in request.POST:
        return route_list(request)
    msg = 'Daemon {} berhasil dihapus.'.format(row.name)
    q.delete()
    DBSession.flush()
    request.session.flash(msg)
    return route_list(request)
