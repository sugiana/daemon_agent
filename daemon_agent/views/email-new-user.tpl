You have registered on our site. Please click the link below to change the
password.

${url}

This link will expire in ${minutes} minutes.
If you did not request this, please ignore it.
