We accepted password recovery requests. Please click the following link:

${url}

This link will expire in ${minutes} minutes.
If you did not request this, please ignore it.
