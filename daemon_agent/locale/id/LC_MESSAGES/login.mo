��          �      �           	                0     >     S     i     o     |     �     �     �     �     �     �     �     �     �               +     ?  =  X     �     �     �     �      �     �               %     :  
   O     Z  +   o     �     �     �     �     �     �  R   �  �   E  u   �                                                                              	      
                    Cancel Change password Forgot password Invalid email Invalid old password Invalid security code Login Login failed New password Old password Password Reset password Retype mismatch Retype new password Save Send password reset email Submit Username change-password-done email-reset-password reset-password-body reset-password-link-sent Project-Id-Version: PACKAGE 1.0
POT-Creation-Date: 2018-10-13 22:28+0700
PO-Revision-Date: 2018-10-13 22:30+0700
Last-Translator: Owo Sugiana <sugiana@gmail.com>
Language-Team: Indonesian
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: Lingua 4.13
 Batalkan Ganti kata kunci Lupa kata kunci Email tidak terdaftar Kata kunci yang lama tidak benar Kode keamananan tidak benar Masuk Gagal masuk Kata kunci yang baru Kata kunci yang lama Kata kunci Pemulihan kata kunci Pengulangan kata kunci yang baru tidak sama Ulangi kata kunci yang baru Simpan Kirim email Kirim Nama Kata kunci Anda telah diubah Tulis email Anda dan kami akan mengirimkan tautan untuk penetapan ulang kata kunci Kami menerima permintaan pemulihan kata sandi. Silakan klik tautan berikut:

${url}

Tautan ini akan kedaluwarsa dalam ${minutes} menit. Mohon abaikan jika Anda tidak memintanya. Periksa email Anda untuk tautan pemulihan kata kunci. Jika tidak muncul dalam beberapa menit, periksa di bagian spam. 