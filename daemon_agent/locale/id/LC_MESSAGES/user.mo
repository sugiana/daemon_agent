��    %      D  5   l      @     A     H     Q     X     _     k     w  	   |     �     �     �  
   �     �     �     �  
   �     �  	   �     �     �                              #     +     /  
   B     M     Z     f     s     �     �     �  =  �               '     0     6  
   E     P     U     c     i     r     w     �     �     �     �     �     �     �     �     �          
            	        )  -   .  ;   \      �     �  #   �  *   �  '     $   C     h     "                                               	                                           $      
                                !   #            %                               Active Add user Cancel Delete Delete user Description Edit Edit user Email Finance Group Group name Groups Inactive Invalid email format Last login Member count No result Registered date Save Show Status System Username Users Warning You email-already-used user-added user-deleted user-result user-updated username-already-used username-first-end-alphanumeric username-only-contain warning-delete-user Project-Id-Version: PACKAGE 1.0
POT-Creation-Date: 2018-10-27 14:20+0700
PO-Revision-Date: 2018-10-27 14:49+0700
Last-Translator: Owo Sugiana <sugiana@gmail.com>
Language-Team: Indonesian
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: Lingua 4.13
 Aktif Tambah pengguna Batalkan Hapus Hapus pengguna Keterangan Ubah Ubah pengguna Email Keuangan Grup Nama kelompok Kelompok Tidak aktif Susunan email tidak benar Terakhir masuk Jumlah anggota Tidak ada hasil Tanggal pendaftaran Simpan Lihat Status Sistem Nama Pengguna Perhatian Anda Email ${email} sudah digunakan oleh ID ${uid} ${email} sudah ditambahkan dan email untuknya sudah dikirim ${email} ID ${uid} sudah dihapus Ada ${count} baris Profil ${username} sudah diperbarui Nama ${username} sudah digunakan ID ${uid} Awal dan akhir hanya boleh a-z atau 0-9 Hanya boleh karakter a-z, 0-9, dan - Hapus ${email} ID ${uid} ? 