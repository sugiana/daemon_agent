#
# Indonesian translations for PACKAGE package
# This file is distributed under the same license as the PACKAGE package.
# Owo Sugiana <sugiana@gmail.com>, 2018.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE 1.0\n"
"POT-Creation-Date: 2018-10-27 14:20+0700\n"
"PO-Revision-Date: 2018-10-27 14:49+0700\n"
"Last-Translator: Owo Sugiana <sugiana@gmail.com>\n"
"Language-Team: Indonesian\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Lingua 4.13\n"

#: web_starter/views/user.py:43
msgid "Users"
msgstr "Pengguna"

#: web_starter/views/user.py:78
msgid "Invalid email format"
msgstr "Susunan email tidak benar"

#. Default: Email ${email} already used by user ID ${uid}
#: web_starter/views/user.py:87
msgid "email-already-used"
msgstr "Email ${email} sudah digunakan oleh ID ${uid}"

#. Default: Only a-z, 0-9, and - characters are allowed
#: web_starter/views/user.py:107
msgid "username-only-contain"
msgstr "Hanya boleh karakter a-z, 0-9, dan -"

#. Default: Only a-z or 0-9 at the start and end
#: web_starter/views/user.py:113
msgid "username-first-end-alphanumeric"
msgstr "Awal dan akhir hanya boleh a-z atau 0-9"

#. Default: Username ${username} already used by ID ${uid}
#: web_starter/views/user.py:122
msgid "username-already-used"
msgstr "Nama ${username} sudah digunakan ID ${uid}"

#: web_starter/views/user.py:141
msgid "Email"
msgstr "Email"

#: web_starter/views/user.py:143
msgid "Username"
msgstr "Nama"

#: web_starter/views/user.py:146
msgid "Group"
msgstr "Grup"

#: web_starter/views/user.py:154
msgid "Status"
msgstr "Status"

#: web_starter/views/user.py:165
msgid "Active"
msgstr "Aktif"

#: web_starter/views/user.py:166
msgid "Inactive"
msgstr "Tidak aktif"

#: web_starter/views/user.py:176
msgid "Save"
msgstr "Simpan"

#: web_starter/views/user.py:177 web_starter/views/user.py:299
msgid "Cancel"
msgstr "Batalkan"

#: web_starter/views/user.py:202
msgid "Add user"
msgstr "Tambah pengguna"

#: web_starter/views/user.py:219
msgid "user-added"
msgstr "${email} sudah ditambahkan dan email untuknya sudah dikirim"

#: web_starter/views/user.py:266
msgid "Edit user"
msgstr "Ubah pengguna"

#. Default: ${username} profile updated
#: web_starter/views/user.py:282
msgid "user-updated"
msgstr "Profil ${username} sudah diperbarui"

#: web_starter/views/user.py:298
msgid "Delete"
msgstr "Hapus"

#: web_starter/views/user.py:303
msgid "Delete user"
msgstr "Hapus pengguna"

#. Default: User ${email} ID ${uid} has been deleted
#: web_starter/views/user.py:306
msgid "user-deleted"
msgstr "${email} ID ${uid} sudah dihapus"

msgid "Registered date"
msgstr "Tanggal pendaftaran"

msgid "Last login"
msgstr "Terakhir masuk"

msgid "Edit"
msgstr "Ubah"

msgid "System"
msgstr "Sistem"

msgid "You"
msgstr "Anda"

msgid "Finance"
msgstr "Keuangan"

msgid "Warning"
msgstr "Perhatian"

msgid "warning-delete-user"
msgstr "Hapus ${email} ID ${uid} ?"

msgid "Show"
msgstr "Lihat"

msgid "user-result"
msgstr "Ada ${count} baris"

msgid "No result"
msgstr "Tidak ada hasil"

msgid "Groups"
msgstr "Kelompok"

msgid "Group name"
msgstr "Nama kelompok"

msgid "Description"
msgstr "Keterangan"

msgid "Member count"
msgstr "Jumlah anggota"
