from subprocess import (
    call,
    PIPE,
    Popen,
    )


def version(name):
    cmd = ['dpkg', '-s', name]
    p = Popen(cmd, stdout=PIPE)
    result = p.communicate()
    s = result[0]
    if isinstance(s, bytes):
        s = s.decode('utf-8')
    for line in s.splitlines():
        if line.find('Version: ') == 0:
            t = line.split()
            return t[1]


if __name__ == '__main__':
    import sys
    package = sys.argv[1]
    ver = version(package)
    if ver:
        print(ver)
