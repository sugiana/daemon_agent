import sys
from datetime import (
    date,
    datetime,
    )
from .waktu import (
    dmy,
    dmyhms,
    )


def to_str(v):
    if isinstance(v, date):
        if isinstance(v, datetime):
            return dmyhms(v)
        return dmy(v)
    if v == 0:
        return '0'
    if isinstance(v, str) or \
        (sys.version_info.major == 2 and isinstance(v, unicode)):
        return v.strip()
    elif isinstance(v, bool):
        return v and '1' or '0'
    return v and str(v) or ''


def dict_to_str(d):
    r = {}
    for key in d:
        val = d[key]
        r[key] = to_str(val)
    return r


