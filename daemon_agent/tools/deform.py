# items = request.POST.items()
def to_dict(items):
    d = dict()
    values = None
    for item in items:
        print(item)
        key, value = item
        if key == '__start__':
            fieldname = value
            values = []
        elif key == '__end__':
            d[fieldname] = values
            values = None
        elif isinstance(values, list):
            values.append(value)
        else:
            d[key] = value
    return d
