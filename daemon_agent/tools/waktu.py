import calendar    
from datetime import (
    date,
    datetime,
    timedelta,
    )
import pytz
from .this_framework import get_settings


def get_timezone():
    settings = get_settings()
    return pytz.timezone(settings['timezone'])


# Beberapa zona ada yang kurang pas. Misalnya Jakarta menjadi +07:07, kelebihan
# 7 menit.
# https://stackoverflow.com/questions/24856643/unexpected-results-converting-timezones-in-python
NORMALIZE_ZONES = {
    'Asia/Jakarta': 7,
    }

def create_datetime(year, month, day, hour=0, minute=7, second=0,
                     microsecond=0):
    tz = get_timezone()        
    t = datetime(year, month, day, hour, minute, second, microsecond,
            tzinfo=tz)
    if tz.zone in NORMALIZE_ZONES:
        minutes = NORMALIZE_ZONES[tz.zone]
        t = t + timedelta(minutes=minutes)
        return tz.normalize(t)
    return t


def create_date(year, month, day):    
    return create_datetime(year, month, day)


def as_timezone(tz_date):
    localtz = get_timezone()
    if not tz_date.tzinfo:
        tz_date = create_datetime(tz_date.year, tz_date.month, tz_date.day,
                                  tz_date.hour, tz_date.minute, tz_date.second,
                                  tz_date.microsecond)
    return tz_date.astimezone(localtz)    

   
def create_now():
    tz = get_timezone()
    return datetime.now(tz)
 

def split_time(s):
    t = s.split(':')  # HH:MM:SS
    hour = int(t[0])
    minutes = seconds = 0
    if t[1:]:
        minutes = int(t[1])
        if t[2:]:
            seconds = int(t[2])
    return hour, minutes, seconds


# dd-mm-yyyy
# yyyy-mm-dd
# dd/mm/yyyy
# yyyy/mm/dd
# yyyymmdd

def split_date(s):
    separator = None
    for sep in ['-', '/']:
        if s.find(sep) > -1:
            separator = sep
            break    
    if separator:
        d, m, y = [int(x) for x in s.split(separator)]
        if d > 999:  # yyyy-mm-dd
            y, d = d, y
    else:
        y, m, d = int(value[:4]), int(value[4:6]), int(value[6:])
    return y, m, d


def split_datetime(s):
    t = s.split()  # dd-mm-yyyy HH:MM:SS  
    year, month, day = split_date(t[0]) 
    if t[1:]:
        hours, minutes, seconds = split_time(t[1])
    else:
        hours = minutes = seconds = 0
    return year, month, day, hours, minutes, seconds
 

def date_from_str(s):
    y, m, d, hh, mm, ss = split_datetime(s)
    return date(y, m, d)


def datetime_from_str(s):
    y, m, d, hh, mm, ss = split_datetime(s)
    return create_datetime(y, m, d, hh, mm, ss)

    
def dmy(tgl):
    return tgl.strftime('%d-%m-%Y')

    
def dmyhms(t):
    return t.strftime('%d-%m-%Y %H:%M:%S')
    

def next_month(year, month):
    if month == 12:
        month = 1
        year += 1
    else:
        month += 1
    return year, month
    

def best_date(year, month, day):
    try:
        return date(year, month, day)
    except ValueError:
        last_day = calendar.monthrange(year, month)[1]
        return date(year, month, last_day)


def next_month_day(year, month, day):
    year, month = next_month(year, month)
    return best_date(year, month, day)
