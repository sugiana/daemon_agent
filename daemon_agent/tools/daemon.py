import os
from subprocess import (
    Popen,
    PIPE,
    )


def get_pid(pid_file):
    try:
        f = open(pid_file)
        pid_int = int(f.read().split()[0])
        f.close()
        return pid_int
    except IOError:
        return
    except ValueError:
        return
    except IndexError:
        return


def is_live(pid):
    try:
        os.kill(pid, 0)
    except OSError:
        return
    for i in range(3):
        p1 = Popen(['ps', 'ax'], stdout=PIPE)
        p2 = Popen(
                ['grep', '^%s%d' % (' ' * i, pid)], stdin=p1.stdout,
                stdout=PIPE)
        p3 = Popen(['grep', '-v', 'grep'], stdin=p2.stdout, stdout=PIPE)
        s = p3.communicate()
        s = s[0]
        if s:
            return pid


