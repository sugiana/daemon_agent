from .models import DBSession
from .models.ziggurat import (
    User,
    UserGroup,
    )


def group_finder(login, request):
    q = DBSession.query(User).filter_by(id=login)
    u = q.first()
    if not u or not u.status:
        return  # None means logout
    if u.id == 1:
        return ['group:1']
    r = []
    q = DBSession.query(UserGroup).filter_by(user_id=u.id)
    for ug in q:
        acl_name = 'group:{}'.format(ug.group_id)
        r.append(acl_name)
    return r


def get_user(request):
    uid = request.authenticated_userid
    if uid:
        q = DBSession.query(User).filter_by(id=uid)
        return q.first()
