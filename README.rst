Daemon Agent
============

Ini merupakan pemantau aktifitas daemon di localhost. Terlebih dahulu pasang
paket Debian yang dibutuhkan::

    $ sudo apt-get install build-essential python-dev

Cari tahu versi Postgres::

    $ dpkg -l | grep postgresql

Lalu akan muncul seperti ini::

    ii postgresql-9.5

Sehingga yang perlu dipasang::

    $ sudo apt-get install postgresql-server-dev-9.5

Buat Python Virtual Environment::

    $ python3 -mvenv ../env

Pasang paket terkait::

    $ ../env/bin/python setup.py develop-use-pip

Salin contoh konfigurasi::

    $ cp development.ini test.ini

Sesuaikan konfigurasi pada baris berikut ini::

    sqlalchemy.url = postgresql://user:pass@localhost/db
    session.url = postgresql://user:pass@localhost/db
   
Buat tabelnya::

    $ ../env/bin/initialize_daemon_agent_db test.ini

Jalankan daemon-nya sebagai root karena terkait pembacaan sistem::

    $ sudo ../env/bin/pserve --reload test.ini
